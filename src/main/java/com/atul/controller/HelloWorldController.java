package com.atul.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.atul.model.ConnectRequest;
import com.atul.model.ConnectResponse;
import com.atul.model.RequestPacket;
import com.atul.model.ResponsePacket;
import com.atul.utility.AESUtils;
import com.atul.utility.RSAUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class HelloWorldController {
	
	@Autowired
	private Environment env;
	
	@Autowired
	private AESUtils aesUtils;
	

	@RequestMapping({ "/hello" })
	public String firstPage() {
		return "Hello World";
	}

	@PostMapping("/connectToServer")
	public ResponseEntity<?> getConnectToServer(@RequestBody ConnectRequest coRequest) {
		boolean isConnected = Boolean.valueOf(env.getProperty("spoofing.status"));
		String random = "";
		if (isConnected == true) {
			random = coRequest.getStatus();
		} else {
			random = coRequest.getStatus() + String.valueOf(Math.round(Math.random()*100));
		}

		return ResponseEntity.ok(new ConnectResponse(random));
	}
	
	@PostMapping("/checkRequest")
	public ResponseEntity<?> getCheckRequest(@RequestBody RequestPacket requestPacket)
	{
		System.out.println(requestPacket);
		String encryptedData = "";
		try {
			String aesKey = RSAUtils.rsaDecryption(requestPacket.getSrc().toString().trim());
			System.out.println(aesKey);
			String decryptData = aesUtils.decrypt(aesKey, requestPacket.getData().trim());
			HashMap<String, String> data = new HashMap<String, String>();
			ObjectMapper mapper = new ObjectMapper();
			try {
				data = mapper.readValue(decryptData, new TypeReference<HashMap<String, String>>() {});
				System.out.println(data);
			} catch (Exception e) {
				e.printStackTrace();
			}
			data.put("hey", "decrypted");
		    encryptedData = aesUtils.encrypt(aesKey, data.toString());
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok(new ResponsePacket(encryptedData));
	}

}