package com.atul.utility;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class RSAUtils {

	private static  String publicKey = "MIIBITANBgkqhkiG9w0BAQEFAAOCAQ4AMIIBCQKCAQBM7MpOOaNUt8YnhtviupsjQYQX4YmPhUFURm4KozFGZ3Gs+Dx1WfMoaKKrGDCijM9PgkYeKZuwIaNa2GaUlemL32dMruVDkTQvcBULv3mSpnEwMIoDsRLgpIXjOOGf28+/QvIksLjEX7FXHDdPz6XfTODWGkbIDMxl9e0WTBus+eNgM9+OPvXj6DI1H9cf2F92fk12slrjJNQTpyri1oUBcwreG46BIoJu/Uvx/7tc26zcN1HJPr4MXY4EuYHhbCX4qYar8wQnYNqngZO5eLhzNpGHeVFBM/r0q81Irls9BUQKRVWZ+KMRFOeG5Dv0hegyT5A1POLR0ViEcbR24VZtAgMBAAE=";
	private static  String privateKey = "MIIEuwIBADANBgkqhkiG9w0BAQEFAASCBKUwggShAgEAAoIBAEzsyk45o1S3xieG2+K6myNBhBfhiY+FQVRGbgqjMUZncaz4PHVZ8yhooqsYMKKMz0+CRh4pm7Aho1rYZpSV6YvfZ0yu5UORNC9wFQu/eZKmcTAwigOxEuCkheM44Z/bz79C8iSwuMRfsVccN0/Ppd9M4NYaRsgMzGX17RZMG6z542Az344+9ePoMjUf1x/YX3Z+TXayWuMk1BOnKuLWhQFzCt4bjoEigm79S/H/u1zbrNw3Uck+vgxdjgS5geFsJfiphqvzBCdg2qeBk7l4uHM2kYd5UUEz+vSrzUiuWz0FRApFVZn4oxEU54bkO/SF6DJPkDU84tHRWIRxtHbhVm0CAwEAAQKCAQAtnH18Eor48Zqp1znL3w+bwP4c1tsk4UNSQAyBfC/8aduqTuoyPuqBvEEvp8E2sL0/jKQcwFkS/28Hr6ZrVdRL3mQ2wMEp5hAGTLP96kOgo9YbV7yN4dGqp4LHrvOBQOmWo5BGFw8HSSIy34UgaqQUmlX6PUxY09XiYwZ4IRqL3lz79B2faOVmY95gwXQnAcbGEX7Q+AJGioVkSVuNsGBF9lCddVhfpsHx4Dwl5ErAnCDkrYUy/xEB74E78803pENm2kjB8RGOsTk1S/LImv5g7j9TjM6rdyqZ+H4GOvPKGqLickO5BrMh9k93YO6t9oNU8wf5i73dPXNuGdCEq8GhAoGBAIzF5JfaMESJmlzYDYmdsHENEZOCH+s0EO5oQO6B/wXBwFyex/bxdwKBwcYqSdk+XA7PN6DwctDOLUMeIc5PlEEm6dETc+1+O7hiHs7Pj8n2WpjWc7dhH6zV/6cyQ6IMBbK4qFmtfuA2G2owTvETAeR9JijDcxu7AvFOOCAy2AiZAoGBAIvj74VX0rYFjiVxNI3REQoq8W8o3jqKx5VHRW3XD/7NScwSh2g6HkpRDS7YV5KtEYeo4tLHIEPoMXHA1/PK286IGRVqXnSL9/YOWxtes7bn7AmmbyhqlbfDRxICaNbRIMplYEFpRJAJ0y9CnoCFPMcA5pHz2UofeVDV6hyRD3z1AoGAZw5w3SJkd6htE08wCfEhPIIevehjSaMiSgaUkocklahUFPpA1e3L/E/V9ib7TUkzx7u3s+CTjX2C8UHb6dekZ1X/koo1MkZZnhBnEWwujeIGTSGiMTuvHq7DrDlF/hnjCgXZaV10Jtw1kbWv9Ri/J/DewLTlTgXIj7N9r0TJlvECgYAlMbCQop4quWVm/Sd9AXZnXq2c4Z/cruOMBEwofUMKe2jAsrh/9Nvy5IP9zZXTMOL1T+knI7yncYgb96szbd9tlXAd6o88q659JKoCp/ZejmGK2064z0YJewldd3iKjUYLqlKicavCJGqbKTpdb4+OVeX4Ln66n42w+X9qowMcNQKBgH11ZhOP6ZVvbfv0xPLzxjX8OPkvq7wWHWWgt6cLodTf9lJyO5UAAXo0ltJIX4GXhxEbsNHXoAVnVdXT29eXi6ktfm+QwmSbGZ1R9G0ENKfGS5gcbBGbFXgLa8A+LHQzaQ/BtxuMHdPssnsilCczJINDMRlGdeRCJLhfdHTzdZfp";

    
    public static PublicKey getPublicKey(String base64PublicKey){
        PublicKey publicKey = null;
        try{
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(base64PublicKey.getBytes()));
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            publicKey = keyFactory.generatePublic(keySpec);
            return publicKey;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return publicKey;
    }

    public static PrivateKey getPrivateKey(String base64PrivateKey){
        PrivateKey privateKey = null;
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(base64PrivateKey.getBytes()));
        KeyFactory keyFactory = null;
        try {
            keyFactory = KeyFactory.getInstance("RSA");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            privateKey = keyFactory.generatePrivate(keySpec);
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return privateKey;
    }

    public static byte[] encrypt(String data, String publicKey) throws BadPaddingException, IllegalBlockSizeException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException {
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, getPublicKey(publicKey));
        return cipher.doFinal(data.getBytes());
    }

    public static String decrypt(byte[] data, PrivateKey privateKey) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        return new String(cipher.doFinal(data));
    }

    public static String decrypt(String data, String base64PrivateKey) throws IllegalBlockSizeException, InvalidKeyException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        return decrypt(Base64.getDecoder().decode(data.getBytes()), getPrivateKey(base64PrivateKey));
    }
    
    public static String rsaEncryption(String data) throws InvalidKeyException, BadPaddingException, IllegalBlockSizeException, NoSuchPaddingException, NoSuchAlgorithmException
    {
    	String encryptedString = Base64.getEncoder().encodeToString(encrypt(data, publicKey));
    	return encryptedString;
    }
    public static String rsaDecryption(String data) throws InvalidKeyException, BadPaddingException, IllegalBlockSizeException, NoSuchPaddingException, NoSuchAlgorithmException
    {
    	String decryptedString = RSAUtils.decrypt(data, privateKey);
    	return decryptedString;
    }

    public static void main(String[] args) throws IllegalBlockSizeException, InvalidKeyException, NoSuchPaddingException, BadPaddingException {
        try {
        	String ecnData = rsaEncryption("089e1803833e1187959a05be53030875");
            System.out.println(ecnData);
            String decData = rsaDecryption(ecnData);
            System.out.println(decData);
        } catch (NoSuchAlgorithmException e) {
            System.err.println(e.getMessage());
        }

    }
}
