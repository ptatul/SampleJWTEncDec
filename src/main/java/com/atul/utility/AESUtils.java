package com.atul.utility;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.springframework.stereotype.Service;

@Service
public class AESUtils {
	private final String EXCEPTION_MESSAGE = "AESUtils error: ";
	private final Cipher cipher;
	static int iterationCount = 1000;
	static int keySize = 128;
	String iv = "089e1803833e1187959a05be53030875";
	String salt = "3ce4c1fbd9f92863c8bb29a8b293188f";
	String passPhrase = "1f491ea331dafff3bd8c38e3c3830e97";

	public AESUtils() {
		this(keySize, iterationCount);
	}

	public AESUtils(int keySize, int iterationCount) {
		this.keySize = keySize;
		this.iterationCount = iterationCount;
		try {
			cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			throw failure(e);
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
			throw failure(e);
		}
	}

	public String encrypt(String plaintext) {
		// return encrypt(salt, iv, passPhrase, plaintext);
		return encrypt("b914741dca31bfae4c2d4d003780b00e", iv, plaintext);
	}
	
	public String encrypt(String key, String iv, String plaintext) {
		try {
			System.out.println("iv: " + iv);
			byte[] encrypted = doFinal(Cipher.ENCRYPT_MODE, generateKey(key), iv, plaintext.getBytes("UTF-8"));
			return base64(encrypted);
		} catch (UnsupportedEncodingException e) {
			throw failure(e);
		}
	}
	
	public String encrypt(String key, String plaintext) {
		try {
			byte[] encrypted = doFinal(Cipher.ENCRYPT_MODE, generateKey(key), iv, plaintext.getBytes("UTF-8"));
			return base64(encrypted);
		} catch (UnsupportedEncodingException e) {
			throw failure(e);
		}
	}

	public String encrypt(String salt, String iv, String passphrase, String plaintext) {
		try {
			SecretKey key = generateKey(salt, passphrase);
			System.out.println("encrypt key hex: " + hex(key.getEncoded()));
			byte[] encrypted = doFinal(Cipher.ENCRYPT_MODE, key, iv, plaintext.getBytes("UTF-8"));
			return base64(encrypted);
		} catch (UnsupportedEncodingException e) {
			throw failure(e);
		}
	}

	public String decrypt(String ciphertext) {
		// return decrypt(salt, iv, passPhrase, ciphertext);
		return decrypt("b914741dca31bfae4c2d4d003780b00e", iv, ciphertext);
	}
	
	public String decrypt(String key, String iv, String ciphertext) {
		try {
			System.out.println("iv: " + iv);
			byte[] decrypted = doFinal(Cipher.DECRYPT_MODE, generateKey(key), iv, base64(ciphertext));
			return new String(decrypted, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw failure(e);
		}
	}

	public String decrypt(String key, String ciphertext) {
		System.out.println("AESUtils.decrypt()"+key+" : "+ciphertext);
		try {
			byte[] decrypted = doFinal(Cipher.DECRYPT_MODE, generateKey(key), iv, base64(ciphertext));
			return new String(decrypted, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw failure(e);
		}
	}

	
	public String decrypt(String salt, String iv, String passphrase, String ciphertext) {
		try {
			SecretKey key = generateKey(salt, passphrase);
			System.out.println("decrypt key hex: " + hex(key.getEncoded()));
			byte[] decrypted = doFinal(Cipher.DECRYPT_MODE, key, iv, base64(ciphertext));
			return new String(decrypted, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw failure(e);
		}
	}

	private byte[] doFinal(int encryptMode, SecretKey key, String iv, byte[] bytes) {
		try {
			cipher.init(encryptMode, key, new IvParameterSpec(hex(iv)));
			return cipher.doFinal(bytes);
		} catch (InvalidKeyException e) {
			e.printStackTrace();
			throw failure(e);
		} catch (InvalidAlgorithmParameterException e) {
			e.printStackTrace();
			throw failure(e);
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
			throw failure(e);
		} catch (BadPaddingException e) {
			e.printStackTrace();
			throw failure(e);
		}
	}
	
	private SecretKey generateKey(String key) {
		return new SecretKeySpec(hex(key), "AES");
	}

	public String generateHexKey(String salt, String passphrase) {
		SecretKey key = generateKey(salt, passphrase);
		return hex(key.getEncoded());
	}

	private SecretKey generateKey(String salt, String passphrase) {
		try {
			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			KeySpec spec = new PBEKeySpec(passphrase.toCharArray(), hex(salt), iterationCount, keySize);
			SecretKey key = new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");
			return key;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			throw failure(e);
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
			throw failure(e);
		}
	}

	public static String randomHex(int length) {
		byte[] randomHex = new byte[length];
		new SecureRandom().nextBytes(randomHex);
		return hex(randomHex);
	}

	public static String base64(byte[] bytes) {
		return new String(Base64.encodeBase64(bytes)); 
	}

	public static byte[] base64(String str) {
		return Base64.decodeBase64(str.getBytes());
	}

	public static String hex(byte[] bytes) {
		return new String(Hex.encodeHex(bytes));
	}

	public static byte[] hex(String str) {
		try {
			return Hex.decodeHex(str.toCharArray());
		} catch (DecoderException e) {
			throw new IllegalStateException(e);
		}
	}

	private IllegalStateException failure(Exception e) {
		return new IllegalStateException(e);
	}

	 public static void main(String[] args) {
	 	String plaintext = "{\"customerId\":\"5220511\",\"deviceId\":\"07575a47950dd519\",\"uuid\":\"07575a47950dd519\",\"accountNumber\":\"041990100007349\"}";
	 	String enc = new AESUtils().encrypt("089e1803833e1187959a05be53030875", plaintext);
	 	String dec = new AESUtils().decrypt("089e1803833e1187959a05be53030875", enc);
	 	System.out.println("encrypt from 3 parameter: " + enc);
	 	System.out.println("decrypt from 3 parameter: " + dec);		
	 }
}
