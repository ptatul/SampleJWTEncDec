package com.atul.model;

public class RequestPacket {
	
	private String data;
	private String src;
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getSrc() {
		return src;
	}
	public void setSrc(String src) {
		this.src = src;
	}
	@Override
	public String toString() {
		return "RequestPacket [data=" + data + ", src=" + src + "]";
	}
	
	

}
