package com.atul.model;

public class ConnectResponseParameter {
	public boolean isConnected;

	public ConnectResponseParameter(boolean isConnected) {
		this.isConnected = isConnected;
	}
}