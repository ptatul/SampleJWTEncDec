package com.atul.model;

public class ResponsePacket {
	private String data;
	
	public ResponsePacket(String data) {
		super();
		this.data = data;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

}
