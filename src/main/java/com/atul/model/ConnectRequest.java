package com.atul.model;

import java.io.Serializable;

public class ConnectRequest implements Serializable {

	private static final long serialVersionUID = -8091879091924046844L;

	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}